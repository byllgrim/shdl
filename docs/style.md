# Coding style guideline

Why:
Because I don't know how to write good Haskell and I want to structure my thoughts around it.


## Rules

Functions:
* Top-level function names start with a verb.
* Local functions names are nouns.

Types:
* Never(?) use standard types (use "data" and "newtype").


## Verbs

Inspired by "TheLOP", I am curious if a finite set of verbs will make things easier or not.

* Parse
* Expect
* Verify
* Clean
* Get
* Make
