{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE Safe #-}

module Main (main) where

import Prelude(
  String, Show, Eq, IO, Bool, Char, getContents, ($), map, words, putStr,
  error, head, (==), otherwise, (++), tail, concatMap, (.), filter, elem,
  Semigroup, show, (||)
  )
import Data.List qualified as DL
import Data.Text qualified as DT
import Data.Monoid qualified as DM
import Data.Monoid ((<>))


-- data (parsing)

data ShdlModule = ShdlModule {
  name :: String,
  body :: ShdlBody
  }
  deriving stock (Show)

data ShdlItem = Ports [ShdlPort] | ShdlInsts [ShdlInst] | ShdlConns [ShdlConn]
  deriving stock (Show)

data ShdlInst = ShdlInst {
  modname :: String,
  instname :: String
  }
  deriving stock (Show)

data ShdlConn = ShdlConn {
  dst :: String,
  src :: String
  }
  deriving stock (Show)


-- newtypes (parsing)

newtype ShdlPort = ShdlPort {unShdlPort :: String}
  deriving stock (Show)

newtype ShdlToken = ShdlToken {unShdlToken :: String}
  deriving stock (Eq)

newtype ShdlBody = ShdlBody {unShdlBody :: [ShdlItem]}
  deriving stock (Show)


-- data (verilogifying)

data VeriInst = VeriInst {
  inst :: ShdlInst,
  conn :: [VeriConn]
  }

data VeriConn = VeriConn {
  port :: String,
  signal :: String
  }


-- newtypes (verilogifying)

newtype VeriCode = VeriCode {unVeriCode :: String}
  deriving stock (Show)
instance Semigroup VeriCode where
  (VeriCode x) <> (VeriCode y) = VeriCode (x <> y)
instance DM.Monoid VeriCode where
  mappend = (<>)
  mempty = VeriCode ""


-- functions (main)

main :: IO ()
main = do
  contents <- getContents
  let m = parseShdlModule $ map ShdlToken $ words contents
  putStr $ unVeriCode $ verifyModule m


-- functions (parsing)

parseShdlModule :: [ShdlToken] -> ShdlModule
parseShdlModule toks = makeShdlModule $ expect "module" toks
  where
    makeShdlModule :: [ShdlToken] -> ShdlModule
    makeShdlModule (tokhead:toktail) = ShdlModule (unShdlToken tokhead) $ parseShdlBody toktail
    makeShdlModule _ = error "error: failed to parse module"

parseShdlBody :: [ShdlToken] -> ShdlBody
parseShdlBody toks = makeShdlBody $ parseShdlItems $ expect "{" toks
  where
    makeShdlBody :: ([ShdlItem], [ShdlToken]) -> ShdlBody
    makeShdlBody (is, _) = ShdlBody is

parseShdlItems :: [ShdlToken] -> ([ShdlItem], [ShdlToken])
parseShdlItems toks = accShdlItems ([], toks)
  where
    accShdlItems :: ([ShdlItem], [ShdlToken]) -> ([ShdlItem], [ShdlToken])
    accShdlItems (its, toksin)
      | head toksin == ShdlToken "}"
        = (its, toksin)
      | head toksin == ShdlToken "ports"
        = collectShdlItems $ parsePorts $ expect "{" $ expect "ports" toksin
      | head toksin == ShdlToken "instances"
        = collectShdlItems $ parseShdlInsts $ expect "{" $ expect "instances" toksin
      | head toksin == ShdlToken "connections"
        = collectShdlItems $ parseShdlConns $ expect "{" $ expect "connections" toksin
      | otherwise
        = error $ "error: unexpected '" ++ unShdlToken (head toksin) ++ "'"
      where
        collectShdlItems :: (ShdlItem, [ShdlToken]) -> ([ShdlItem], [ShdlToken])
        collectShdlItems (i, toksremaining) = accShdlItems (i:its, toksremaining)

parsePorts :: [ShdlToken] -> (ShdlItem, [ShdlToken])
parsePorts toks = accPorts (Ports [], toks)
  where
    accPorts :: (ShdlItem, [ShdlToken]) -> (ShdlItem, [ShdlToken])
    accPorts (Ports ps, toksin)
      | head toksin == ShdlToken "}" = (Ports ps, tail toksin)
      | otherwise = accPorts $ collectPorts $ parsePort toksin
      where
        collectPorts :: (ShdlPort, [ShdlToken]) -> (ShdlItem, [ShdlToken])
        collectPorts (p, toksremaining) = (Ports (p:ps), toksremaining)
    accPorts _ = error "error: failed to parse ports"

parsePort :: [ShdlToken] -> (ShdlPort, [ShdlToken])
parsePort = accPort $ ShdlPort ""
  where
    accPort :: ShdlPort -> [ShdlToken] -> (ShdlPort, [ShdlToken])
    accPort acc (t:ts)
      | t == ShdlToken ";" = (acc, ts)
      | otherwise = accPort (ShdlPort (unShdlPort acc ++ " " ++ unShdlToken t)) ts
    accPort _ _ = error "error: failed to parse port"

parseShdlInsts :: [ShdlToken] -> (ShdlItem, [ShdlToken])
parseShdlInsts toks = accShdlInsts (ShdlInsts [], toks)
  where
    accShdlInsts :: (ShdlItem, [ShdlToken]) -> (ShdlItem, [ShdlToken])
    accShdlInsts (ShdlInsts is, toksin)
      | head toksin == ShdlToken "}" = (ShdlInsts is, tail toksin)
      | otherwise = accShdlInsts $ collectShdlInsts $ parseShdlInst toksin
      where
        collectShdlInsts :: (ShdlInst, [ShdlToken]) -> (ShdlItem, [ShdlToken])
        collectShdlInsts (i, toksremaining) = (ShdlInsts (i:is), toksremaining)
    accShdlInsts _ = error "error: failed to accumulate instances"

parseShdlInst :: [ShdlToken] -> (ShdlInst, [ShdlToken])
parseShdlInst (mname:iname:_:toks) = (ShdlInst (unShdlToken mname) (unShdlToken iname), toks)
parseShdlInst _ = error "error: parsing instance failed"

parseShdlConns :: [ShdlToken] -> (ShdlItem, [ShdlToken])
parseShdlConns toks = accShdlConns (ShdlConns [], toks)
  where
    accShdlConns :: (ShdlItem, [ShdlToken]) -> (ShdlItem, [ShdlToken])
    accShdlConns (ShdlConns cs, dest:(ShdlToken "="):source:(ShdlToken ";"):toksremain)
      = accShdlConns (ShdlConns (ShdlConn (unShdlToken dest) (unShdlToken source) : cs), toksremain)
    accShdlConns (cs, (ShdlToken "}"):toksremain)
      = (cs, toksremain)
    accShdlConns _ = error "error: accumulating conns failed"

expect :: String -> [ShdlToken] -> [ShdlToken]
expect w (t:toks)
  | w == unShdlToken t = toks
  | otherwise = error $ "error: expected '" ++ w ++ "' got '" ++ unShdlToken t ++ "'"
expect _ _ = error "error: expected the unexpected"


-- functions (verilogifying)

verifyModule :: ShdlModule -> VeriCode
verifyModule m = header <> wires <> assigns <> instances <> end
  where
    header :: VeriCode
    header = VeriCode $ "module " ++ name m ++ " (\n" ++ unVeriCode (verifyPorts (unShdlBody $ body m)) ++ ");\n\n"
    wires :: VeriCode
    wires = verifyWires (getShdlConns (unShdlBody $ body m)) <> VeriCode "\n"
    assigns :: VeriCode
    assigns = verifyAssigns (getShdlConns (unShdlBody $ body m)) <> VeriCode "\n"
    instances :: VeriCode
    instances = VeriCode $ concatMap (unVeriCode . verifyInst) (getVeriInsts m) ++ "\n"
    end :: VeriCode
    end = VeriCode "endmodule\n"

verifyAssigns :: [ShdlConn] -> VeriCode
verifyAssigns cs = VeriCode $ concatMap todo $ filter nonHier cs
  where
    todo :: ShdlConn -> String
    todo c = "assign " ++ dst c ++ " = " ++ src (cleanName c) ++ ";\n"
    nonHier :: ShdlConn -> Bool
    nonHier c = '.' `DL.notElem` dst c

verifyWires :: [ShdlConn] -> VeriCode
verifyWires cons = VeriCode $ (concatMap (unVeriCode . connToWire) . cleanSourceNames . nestedSources) cons
  where
    nestedSources :: [ShdlConn] -> [ShdlConn]
    nestedSources cs = filter isNested cs
      where
        isNested :: ShdlConn -> Bool
        isNested c = elem '.' $ src c
    connToWire :: ShdlConn -> VeriCode
    connToWire c = VeriCode $ "wire " ++ src c ++ ";\n"

verifyPorts :: [ShdlItem] -> VeriCode
verifyPorts is = DM.mconcat (map portToVerilog is) <> VeriCode "\n"
  where
    portToVerilog :: ShdlItem -> VeriCode
    portToVerilog (Ports ps) = VeriCode $ DL.intercalate ",\n" $ map unShdlPort ps
    portToVerilog _ = DM.mempty

makeVeriConns :: [VeriConn] -> VeriCode
makeVeriConns cs = DM.mconcat $ DL.intersperse (VeriCode ",\n") $ map connection cs
  where
    connection :: VeriConn -> VeriCode
    connection c = VeriCode $ "." ++ port c ++ "(" ++ signal c ++ ")"

verifyInst :: VeriInst -> VeriCode
verifyInst i = declaration <> VeriCode " (\n" <> conns <> VeriCode "\n);\n"
  where
    declaration :: VeriCode
    declaration = VeriCode $ modname (inst i) ++ " " ++ instname (inst i)
    conns :: VeriCode
    conns = makeVeriConns $ conn i

cleanSourceNames :: [ShdlConn] -> [ShdlConn]
cleanSourceNames = map cleanName

cleanName :: ShdlConn -> ShdlConn
cleanName (ShdlConn dest source) = ShdlConn dest $ concatMap dotToScore source
  where
    dotToScore :: Char -> String
    dotToScore c
      | c == '.' = "__"
      | otherwise = [c]

getShdlConns :: [ShdlItem] -> [ShdlConn]
getShdlConns = concatMap getInnerShdlConns
  where
    getInnerShdlConns (ShdlConns cs) = cs
    getInnerShdlConns _ = []

getVeriInsts :: ShdlModule -> [VeriInst]
getVeriInsts m = map (makeVeriInst shdlcons) $ concatMap getShdlInstList $ unShdlBody $ body m
  where
    getShdlInstList :: ShdlItem -> [ShdlInst]
    getShdlInstList (ShdlInsts is) = is
    getShdlInstList _ = []
    shdlcons :: [ShdlConn]
    shdlcons = getShdlConns $ unShdlBody $ body m

getConnsOfInst :: [ShdlConn] -> ShdlInst -> [ShdlConn]
getConnsOfInst cs i = filter (instnameMatch i) cs
  where
    instnameMatch :: ShdlInst -> ShdlConn -> Bool
    instnameMatch ins c = (getDestInstName c == instname ins) || (getSrcInstName c == instname ins)

getSrcInstName :: ShdlConn -> String
getSrcInstName c = DT.unpack $ head $ DT.splitOn (DT.pack ".") (DT.pack $ src c)

getDestInstName :: ShdlConn -> String
getDestInstName c = DT.unpack $ head $ DT.splitOn (DT.pack ".") (DT.pack $ dst c)

getSecondDotItem :: String -> String
getSecondDotItem s = DT.unpack $ second $ DT.splitOn (DT.pack ".") (DT.pack s)
  where
    second :: [DT.Text] -> DT.Text
    second (_:b:_) = b
    second ts = error $ "could not get second item from" ++ show ts

makeVeriInst :: [ShdlConn] -> ShdlInst -> VeriInst
makeVeriInst scons i
  = VeriInst i $ map (makeVeriConn . makeWireOutports i . makeIndexless) $ getConnsOfInst scons i

makeIndexless :: ShdlConn -> ShdlConn
makeIndexless c = ShdlConn (removeIndex $ dst c) (removeIndex $ src c)
  where
    removeIndex :: String -> String
    removeIndex s = DT.unpack $ head $ DT.splitOn (DT.pack "[") $ DT.pack s

makeWireOutports :: ShdlInst -> ShdlConn -> ShdlConn
makeWireOutports i c
  | instname i == getSrcInstName c = ShdlConn (src c) (src c)
  | otherwise = c

makeVeriConn :: ShdlConn -> VeriConn
makeVeriConn = (\c -> VeriConn (getSecondDotItem $ dst c) (src c)) . cleanName
