#!/usr/bin/env python3

import sys
import re

lines = []
indents = []

for line in sys.stdin:
    lines.append(line)

indents.append('')
for line in lines:
    line = line.rstrip()
    lead_space = re.match(' *', line)[0]
    tail = re.sub('^ *', '', line)

    prev_indent = indents[-1]
    is_prev_indent = len(prev_indent) > 0
    is_empty_line = len(line) <= 0
    is_indent = len(prev_indent) < len(lead_space)
    is_dedent = len(lead_space) < len(prev_indent)
    bracket = ''

    if is_empty_line:
        pass
    elif is_indent:
        bracket = ' { '
        indents.append(lead_space)
    elif is_dedent:
        bracket = ' ; } '
        indents.pop()
        # TODO should repeat for each applicable indents[i]
    elif is_prev_indent:
        bracket = ' ; '

    print(lead_space + bracket + tail)
print(' ; ')

for i in range(1, len(indents)):
    print(' } ')
