#!/usr/bin/env python3

import sys
import json
import pyparsing as pp

text = sys.stdin.read()

db_name = ''
db_ports = []
db_instances = []
db_connections = []


def portify(s, loc, toks):
    toks_cut = toks[0:-1]
    db_ports.append(toks_cut)


def instantiate(s, loc, toks):
    toks_cut = toks[0:-1]
    db_instances.append(toks_cut)


def connect(s, loc, toks):
    toks_cut = toks[0:-1]
    db_connections.append(toks_cut)


def module_name(s, loc, toks):
    global db_name
    db_name = toks[0]


indent = '{'
dedent = '}'
newline = ';'
freeline = pp.ZeroOrMore(pp.Word(pp.alphas) | pp.oneOf('. = _')) + newline
port = freeline.copy().setParseAction(portify)
ports = pp.Literal('ports') + indent + pp.OneOrMore(port) + dedent
instance = freeline.copy().setParseAction(instantiate)
instances = pp.Literal('instances') + indent + pp.OneOrMore(instance) + dedent
connection = freeline.copy().setParseAction(connect)
connections = pp.Literal('connections') + indent + \
    pp.OneOrMore(connection) + dedent
items = ports | instances | connections
body = indent + pp.OneOrMore(items) + dedent
module = pp.ZeroOrMore(newline) + 'module' + \
    pp.Word(pp.alphas).setParseAction(module_name) + body

# freeline.setDebug()
# ports.setDebug()
# instances.setDebug()
# connections.setDebug()
# items.setDebug()
# body.setDebug()
# module_parser.setDebug()

result = module.parseString(text)


def print_module_header(module_name, port_lists):
    print('module ', end='')
    print(module_name, end='')
    print('(')

    for port_list in port_lists:
        comma = ''
        print(' '.join(port_list), end='')
        if not (port_list == port_lists[-1]):
            comma = ','
        print(comma)

    print(');')


def get_rhs(list):
    rhs = []
    eq_found = False

    for elem in list:
        if eq_found:
            rhs.append(elem)
        if elem == '=':
            eq_found = True

    return rhs


def is_hierarchical(list):
    return '.' in list


def underscorify(list):
    uslist = []

    for elem in list:
        if elem == '.':
            uslist.append('_')
        else:
            uslist.append(elem)

    return uslist


def print_signal_declarations(connection_lists):
    print('')

    rhs_lists = list(map(get_rhs, connection_lists))
    hier_lists = list(filter(is_hierarchical, rhs_lists))
    score_lists = list(map(underscorify, hier_lists))

    for l in score_lists:
        signame = ''.join(l)
        print('wire ' + signame + ';')


def print_instantiations(instance_lists):
    print('')
    for instance_list in instance_lists:
        print(' '.join(instance_list), end='')
        print('(')
        print('/* TODO print port connections */')
        print(');')


print_module_header(db_name, db_ports)
print_signal_declarations(db_connections)
print_instantiations(db_instances)
print('\nendmodule')
