hs: shdl
shdl: shdl.hs
	ghc -Weverything shdl.hs
	hlint shdl.hs

hstest: example.v
example.v: shdl example.shdl
	cat example.shdl | ./brackify/brackify.py | ./shdl | tee example.v

format:
	autopep8 --in-place --aggressive --aggressive *.py

vformat:
	verible-verilog-format --inplace test.v
