# shdl

Structural HDL minilanguage WIP

This is an experimental minilanguage for structural HDL.
It should translate into synthesizable verilog.
The intention is to simplify writing of "structural" modules.
Structural means that it is only for interconnecting and instantiating.


## Example

```
module x
  ports
    input wire clk
  instances
    a ai
    b bi
  connections
    ai.clk = clk
    ai.op_i = bi.res_o
    bi.clk = clk
```


## Grammar (wirth)

```
module      : "module" identifier body
body        : INDENT {item} DEDENT
item        : (ports | instances | connections)
ports       : "ports" INDENT {port} DEDENT
port        : freeline ";"
instances   : "instances" INDENT {instance} DEDENT
instance    : modulename instancename ";"
connections : "connections" INDENT {connection} DEDENT
connection  : [dstinstance "."] dstsignal "=" [srcinstance "."] srcsignal ";"
```
